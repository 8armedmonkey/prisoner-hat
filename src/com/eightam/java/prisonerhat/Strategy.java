package com.eightam.java.prisonerhat;

public interface Strategy {

    Hat process(PrisonersRow prisonersRow, int position);

}
