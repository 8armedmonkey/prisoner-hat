package com.eightam.java.prisonerhat;

import java.util.ArrayList;
import java.util.List;

public class PrisonersRow {

    private List<Prisoner> prisoners;

    public PrisonersRow() {
        prisoners = new ArrayList<>();
    }

    public void add(Prisoner prisoner) {
        prisoners.add(prisoner);
    }

    public Prisoner get(int position) {
        return prisoners.get(position);
    }

    public boolean contains(Prisoner prisoner) {
        return prisoners.contains(prisoner);
    }

    public int count() {
        return prisoners.size();
    }

    public PrisonersRow getPrisonersInFront(int position) {
        PrisonersRow prisonersRow = new PrisonersRow();
        int count = count();
        int nextPosition = position + 1;

        if (nextPosition < count) {
            prisonersRow.prisoners.addAll(prisoners.subList(nextPosition, count));
        }

        return prisonersRow;
    }

}
