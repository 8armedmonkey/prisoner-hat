package com.eightam.java.prisonerhat;

import java.util.Random;

public class Main {

    public static void main(String[] args) {
        PrisonersRow prisonersRow = getPrisonersRow();
        Strategy strategy = new FirstPrisonerCalculationStrategy();

        Game game = new Game(prisonersRow, strategy);
        game.play();

        PrisonerAnswers prisonerAnswers = game.getPrisonerAnswers();
        boolean prisonersSurvive = game.doPrisonersSurvive();

        for (int i = 0, n = prisonersRow.count(); i < n; i++) {
            Prisoner prisoner = prisonersRow.get(i);
            Hat answer = prisonerAnswers.get(prisoner);
            String correctCheck = prisoner.getHat() == answer ? "√" : "x";

            String line = String.format(">>>>> #%2d hat: %6s, answer: %6s [%s]",
                    i,
                    prisoner.getHat().name(),
                    answer.name(),
                    correctCheck);

            System.out.println(line);
        }

        System.out.println(">>>>> Prisoners survive: " + prisonersSurvive);
    }

    private static PrisonersRow getPrisonersRow() {
        Random random = new Random();

        PrisonersRow prisonersRow = new PrisonersRow();
        int prisonersCount = random.nextInt(5) + 5;

        for (int i = 0; i < prisonersCount; i++) {
            Hat hat = random.nextBoolean() ? Hat.WHITE : Hat.BLACK;
            prisonersRow.add(new Prisoner(hat));
        }

        return prisonersRow;
    }

}
