package com.eightam.java.prisonerhat;

import java.util.HashMap;
import java.util.Map;

public class PrisonerAnswers {

    private PrisonersRow prisonersRow;
    private Map<Prisoner, Hat> answers;

    public PrisonerAnswers(PrisonersRow prisonersRow) {
        this.prisonersRow = prisonersRow;
        this.answers = new HashMap<>();
    }

    public Hat get(Prisoner prisoner) {
        return answers.get(prisoner);
    }

    public void set(Prisoner prisoner, Hat hat) {
        if (prisonersRow.contains(prisoner) && !answers.containsKey(prisoner)) {
            answers.put(prisoner, hat);
        }
    }

}
