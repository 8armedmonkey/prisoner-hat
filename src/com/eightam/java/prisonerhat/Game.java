package com.eightam.java.prisonerhat;

public class Game {

    private PrisonersRow prisonersRow;
    private PrisonerAnswers prisonerAnswers;
    private Strategy strategy;

    public Game(PrisonersRow prisonersRow, Strategy strategy) {
        this.prisonersRow = prisonersRow;
        this.prisonerAnswers = new PrisonerAnswers(prisonersRow);
        this.strategy = strategy;
    }

    public void play() {
        for (int i = 0, n = prisonersRow.count(); i < n; i++) {
            Hat hat = strategy.process(prisonersRow, i);
            prisonerAnswers.set(prisonersRow.get(i), hat);
        }
    }

    public PrisonerAnswers getPrisonerAnswers() {
        return prisonerAnswers;
    }

    public boolean doPrisonersSurvive() {
        int prisonersCount = prisonersRow.count();
        int countOfCorrectAnswers = 0;

        for (int i = 0; i < prisonersCount; i++) {
            Prisoner prisoner = prisonersRow.get(i);
            Hat answer = prisonerAnswers.get(prisoner);

            if (prisoner.getHat() == answer) {
                countOfCorrectAnswers++;
            }
        }

        return countOfCorrectAnswers >= prisonersCount - 1;
    }

}
