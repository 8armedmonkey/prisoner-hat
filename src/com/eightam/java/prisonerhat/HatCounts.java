package com.eightam.java.prisonerhat;

public class HatCounts {

    private int whiteHatCount;
    private int blackHatCount;

    public HatCounts(int whiteHatCount, int blackHatCount) {
        this.whiteHatCount = whiteHatCount;
        this.blackHatCount = blackHatCount;
    }

    public void decreaseWhiteHatCount() {
        whiteHatCount = Math.max(0, --whiteHatCount);
    }

    public void decreaseBlackHatCount() {
        blackHatCount = Math.max(0, --blackHatCount);
    }

    public int getWhiteHatCount() {
        return whiteHatCount;
    }

    public int getBlackHatCount() {
        return blackHatCount;
    }

}
