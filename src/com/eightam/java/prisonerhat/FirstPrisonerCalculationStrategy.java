package com.eightam.java.prisonerhat;

public class FirstPrisonerCalculationStrategy implements Strategy {

    private HatCounts hatCounts;

    @Override
    public Hat process(PrisonersRow prisonersRow, int position) {
        PrisonersRow prisonersInFront = prisonersRow.getPrisonersInFront(position);
        HatCounts prisonersInFrontHatCounts = countHats(prisonersInFront);

        Hat answer;

        if (position == 0) {
            hatCounts = prisonersInFrontHatCounts;
            answer = whiteHatCountExpressedAsHat(hatCounts);
        } else {
            Hat currentWhiteHatCountExpressedAsHat = whiteHatCountExpressedAsHat(hatCounts);
            Hat whiteHatCountExpressedAsHat = whiteHatCountExpressedAsHat(prisonersInFrontHatCounts);

            if (currentWhiteHatCountExpressedAsHat != whiteHatCountExpressedAsHat) {
                answer = Hat.WHITE;
                hatCounts.decreaseWhiteHatCount();
            } else {
                answer = Hat.BLACK;
                hatCounts.decreaseBlackHatCount();
            }
        }

        return answer;
    }

    private HatCounts countHats(PrisonersRow prisonersRow) {
        int whiteHatCount = 0;
        int blackHatCount = 0;

        for (int i = 0, n = prisonersRow.count(); i < n; i++) {
            switch (prisonersRow.get(i).getHat()) {
                case WHITE: {
                    whiteHatCount++;
                    break;
                }

                case BLACK: {
                    blackHatCount++;
                    break;
                }
            }
        }

        return new HatCounts(whiteHatCount, blackHatCount);
    }

    private Hat whiteHatCountExpressedAsHat(HatCounts hatCounts) {
        if (hatCounts.getWhiteHatCount() % 2 == 0) {
            return Hat.WHITE;
        } else {
            return Hat.BLACK;
        }
    }

}
