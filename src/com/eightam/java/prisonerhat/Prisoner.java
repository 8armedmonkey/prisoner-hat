package com.eightam.java.prisonerhat;

public class Prisoner {

    private Hat hat;

    public Prisoner(Hat hat) {
        this.hat = hat;
    }

    public Hat getHat() {
        return hat;
    }

}
